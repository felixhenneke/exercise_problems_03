# -*- coding: utf-8 -*-

'''
solve possion eq in two D
The right-hand side and the boundary conditions should be chosen such that:
    
u(x; y) = (x^4)*(y^5) + 17*sin(xy)
  
'''
import numpy as np
import matplotlib.pyplot as plt

class FivePointStencil:
    '''
    This class solve the Poisson equation numerically in 2D
    using the five-point stencil finite difference method.
    '''

    def __init__ (self,nx,ny):
        
        '''
        Parameters:
                    nx (int): the number of grid in the x direction
                    ny (int): the number of grid in the y direction

        '''
        
        self.nx = nx
        self.ny = ny

    def boundary_initialization (self):
        '''
        Initialization of boundary condition based on the right hand side function
        right hand side function: u(x; y) = (x^4)*(y^5) + 17*sin(xy)
        '''
        x_grid = np.linspace(0, 1, self.nx+1)
        y_grid = np.linspace(0, 1, self.ny+1)

        xx, yy = np.meshgrid(x_grid, y_grid, sparse=True)
        
        # Initialize all nodes (inner and outer) by the given function
        values = (np.power(xx, 4))*(np.power(yy, 5)) + np.sin(xx*yy)

        # initialize all inner node by zero
        values  = values[1:-1, 1:-1]
        values = values.flatten()

        # compute hx and hy
        hx =1/self.nx
        hy = 1/self.ny

        # Au - Bg = f
        # compute "A" Matrix   
                
        diag_coeff = 2*((1/hx**2) + (1/hy**2))
        eins = (1/hx**2)*np.ones((self.nx+1,))
        block_matrix = diag_coeff * np.eye((self.nx-1)) 
        off_diag_upper = np.diag(-1*eins, k=1)
        off_diag_lower = np.diag(eins, k=-1)
        
        block_matrix = block_matrix + off_diag_upper + off_diag_lower
        
        corner_matrix = (1/hy**2)*np.eye((self.nx-1)*(self.ny-1))

        half_matrix_upper = np.hstack((block_matrix,corner_matrix))
        half_matrix_lower = np.hstack((corner_matrix,block_matrix))
        A = np.vstack((half_matrix_upper,half_matrix_lower))

        u = np.linalg.solve(A, values)

        return u

if __name__=="__main__":

    nx=4
    ny=3
    fdm_obj = FivePointStencil(nx,ny)
    solution = fdm_obj.boundary_initialization()




